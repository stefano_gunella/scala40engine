package com.gun.scala40engine.utils;

import com.gun.scala40engine.data.deck.Card;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class JsonManager {
	public void writeJSON(List<Card> lCards) {
		JSONArray jArray = new JSONArray();
		for (Card card : lCards) {
			jArray.put(card2Json(card));
		}
		try {
			System.out.println(jArray.toString(3));
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Card> readJSON(String filename) {
		ArrayList<Card> lResult = new ArrayList<Card>();
		try {
			JSONObject jsonObject = new JSONObject(getJsonFromFile(filename));
			JSONArray jsonArray = (org.json.JSONArray) jsonObject.getJSONArray("nomeNODO");
			Card card = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				jsonObject = (org.json.JSONObject)jsonArray.get(i);
				card = new Card();
				card.deck = ((Long)jsonObject.get("deck")).intValue();
				card.suit = (String)jsonObject.get("suit");
				card.value = (String)jsonObject.get("value");
				lResult.add(card);
				System.out.println(card);
			}
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lResult;
	}

	private String getJsonFromFile(String filename) {
		StringBuffer result = new StringBuffer();
		URL location = JsonManager.class.getProtectionDomain().getCodeSource().getLocation();
		String path = location.getPath();
		try {
			FileReader reader = new FileReader(path+"/"+filename);
			BufferedReader br = new BufferedReader(reader);
			String line;
			while ((line = br.readLine()) != null) {
				result.append(line);
			}
			br.close();
		}
		catch (IOException e){
			// to do
		}
		return result.toString();
	}

	public JSONObject card2Json(Card card) {
		JSONObject object = new JSONObject();
		try {
			object.put("deck", Integer.valueOf(card.deck));
			object.put("suit", card.suit);
			object.put("value", card.value);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return object;
	}

	public Card json2Card(JSONObject jcard) {
		Card result = null;
		try {
			int deck = jcard.getInt("deck");
			String suit = jcard.getString("suit");
			String value = jcard.getString("value");
			result = new Card(deck,suit,value);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
}
