package com.gun.scala40engine.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gun.scala40engine.data.deck.Card;

public class GameValidator {
	private static String REGEX_SUIT =	"(?!(?:.*\\*){@J})[A2-9XJQK*@]{3,13}";	// --> http://stackoverflow.com/questions/18463211/regex-string-contains-max-one
	private static String REGEX_VALUE =	"(?!(?:.*\\*){@J})[A2-9XJQK*@]{3,4}";
	private HashMap<String, Integer> suitMap  = new HashMap<String, Integer>(); 	// mappa SEME-Valore
	private HashMap<Integer, String> suitMap2 = new HashMap<Integer, String>();	// mappa Valore-SEME
	private HashMap<String, Integer> valueMap = new HashMap<String, Integer>(); 	// mappa dei Valori
	List<String>[] allGamesForSuit  = new List[4];	// tutti i giochi possibili per seme
	List<String>[] allGamesForValue = new List[13];	// tutti i giochi possibili per valore
	// E'la struttura su cui si basano tutti i calcoli per le possibili combinazioni.
	// public Hashtable<Integer,List<String>> all_valid_games_suit[] = new Hashtable[4];
	public Hashtable<String,String> all_valid_games_suit2[] = new Hashtable[4];
	public Hashtable<String,String> all_valid_games_value[] = new Hashtable[14];
	List<String> list_games_suit = new ArrayList<String>();
	//	Hashtable<String, String> hash_games_suit = new Hashtable();
	//	Hashtable<String, String> hash_games_value = new Hashtable();

	public GameValidator(){
		suitMap.put("Q",Integer.valueOf(0));	// Quadri
		suitMap.put("C",Integer.valueOf(1));	// Cuori
		suitMap.put("P",Integer.valueOf(2));	// Picche
		suitMap.put("F",Integer.valueOf(3));	// Fiori
		suitMap.put("*",Integer.valueOf(4));	// Jolly

		suitMap2.put(Integer.valueOf(0),"Q");	// Quadri
		suitMap2.put(Integer.valueOf(1),"C");	// Cuori
		suitMap2.put(Integer.valueOf(2),"P");	// Picche
		suitMap2.put(Integer.valueOf(3),"F");	// Fiori
		suitMap2.put(Integer.valueOf(4),"*");	// Jolly

		// contiene la posizione della carta nella mappa delle carte
		valueMap.put("A",0);	// A - nella posizione di testa
		valueMap.put("2",1);
		valueMap.put("3",2);
		valueMap.put("4",3);
		valueMap.put("5",4);
		valueMap.put("6",5);
		valueMap.put("7",6);
		valueMap.put("8",7);
		valueMap.put("9",8);
		valueMap.put("X",9);
		valueMap.put("J",10);
		valueMap.put("Q",11);
		valueMap.put("K",12);
		valueMap.put("@",13);	// A - nella posizione di coda
		valueMap.put("*",-1);	// Jolly

		for (int i = 0; i<4; i++) {
//			all_valid_games_suit[i] = new Hashtable<Integer, List<String>>();
			all_valid_games_suit2[i] = new Hashtable<String, String>();			
		}

		for (int i = 0; i<14; i++) {
			all_valid_games_value[i] = new Hashtable();
		}
	}

	/***
	 * Verifica le carte del giocatore player
	 * @param playerCards
	 */
	public void checkPlayerCards(List<Card> playerCards){
		// divido le carte della mano per valore e seme...
		int num_joker = 0;
		StringBuffer temp = new StringBuffer();
		for (Card item : playerCards) {
			temp.append(item.value+item.suit+"|");
			num_joker = "*".equals(item.value) ? num_joker+1:num_joker;
		}
		String carteInManoAlGiocatore = temp.toString();
		System.out.println("JOLLY>"+num_joker);
		System.out.println("ALL_CARDS>"+carteInManoAlGiocatore);
		// Per ogni riga(seme) estraggo tutti i giochi disponibili...
		// questo serve per ricavare i giochi sulle colonne...
		Card[][] allMyCards = new Card[4][14]; 
		for (int iSuit=0; iSuit<4 ;iSuit++) {
			list_games_suit = new ArrayList<String>();
			checkValidGamesSuit(iSuit,carteInManoAlGiocatore,num_joker,true);
			allGamesForSuit[iSuit]=_Hashtable2List(all_valid_games_suit2[iSuit]);
			Card[] carteInManoPerSeme = _playerCardsOnString2MyCards(iSuit,carteInManoAlGiocatore.toString(),null); // dispongo le carte di tutti i semi
			allMyCards[iSuit] = carteInManoPerSeme;
			//			printAllValidGamesSuit(iSuit);
			printAllValidGamesSuit2(iSuit);
		}
		// Per ogni colonna(A234...JQK) estraggo tutti i giochi disponibili...
		Card[] myCardsValue = new Card[4];
		for (int iValue=0;iValue<13;iValue++) {
			for(int j=0;j<4;j++){
				myCardsValue[j] = allMyCards[j][iValue] ;
			}
			checkValidGamesValue(iValue,_getCardStringByValueArray(myCardsValue), num_joker);
			allGamesForValue[iValue]=_Hashtable2List(all_valid_games_value[iValue]);
			printAllValidGamesValue(iValue);
		}
	}

	/***
	 * @param iSuit
	 * @param carteInManoAlGiocatore in formato completo:AF|2F|3F|4F|5F|6F|7F|8F|9F|XF|JF|QF|KF|@F
	 * @param max_jolly
	 */
	int counter = 0;
	public void checkValidGamesSuit(int iSuit, String carteInManoAlGiocatore, int max_jolly, boolean useJolly){
		String carteDaGiocareFormatoCompresso = _getCarteInManoDivisePerSemeTogliendoLaSequenzaGiocataInFormatoCompresso(iSuit, carteInManoAlGiocatore, null);
		carteDaGiocareFormatoCompresso = carteDaGiocareFormatoCompresso.trim(); // es:A23**67**XJ**
		if(max_jolly > 0 ){
			carteDaGiocareFormatoCompresso = carteDaGiocareFormatoCompresso.replaceAll(" ", "*");
		}
		int i_step = 3;
		int i_length = carteDaGiocareFormatoCompresso.length();
		if(i_length<i_step && list_games_suit.size()>0){
			List<String> validGame = new ArrayList<String>(list_games_suit);
			//			all_valid_games_suit[iSuit].put(validGame.hashCode(), validGame);
			all_valid_games_suit2[iSuit].put(listString2StringSuit(validGame,iSuit), listString2StringSuit(validGame,iSuit));
			printListValidGamesValue(validGame);
			return;
		}
		String REGEX = REGEX_SUIT.replace("@J", ""+(max_jolly+1));
		Pattern p = Pattern.compile(REGEX);
		int i_start = 0;
		String carteDaIgnorare;
		String carteDaGiocare;
		for(int step=i_step;step<=i_length;step++){
			for(int i=i_start;i<=i_length-step;i++){
				carteDaIgnorare = carteDaGiocareFormatoCompresso.substring(0,i);
				carteDaGiocare = carteDaGiocareFormatoCompresso.substring(i,i+step);// prendo il primo gruppo di carte
				Matcher m = p.matcher(carteDaGiocare);								// verifico che siano valide
				if(m.find()){
					if(m.group(0).length()>0){
						list_games_suit.add(m.group(0));
					}
					String carteDaGiocareFormatoCompleto = _getCarteInManoDivisePerSemeTogliendoLaSequenzaGiocataInFormatoCompleto(iSuit, carteInManoAlGiocatore, carteDaIgnorare+m.group(0)); // sottraggo le carte appena aggiunte 
					checkValidGamesSuit(iSuit,carteDaGiocareFormatoCompleto, max_jolly, false);
					list_games_suit.remove(m.group(0));
					System.out.print("\n");
				}
				else{ // se non c'e' stata una sequenza con m.find() allora posso saltare tutti i caratteri presi in considerazione tranne gli ultimi 2
					i = (i<=i_length-step) ? i+i_length-2 : i;
				}
			}
		}
		return;
	}

	/***
	 * Verifica i giochi validi per valore:
	 * Se arriva una serie di ASSI del tipo "A**A" il validatore per Seme non e' piu' in grado
	 * di stabilire se il gioco e' valido - non riconosce la sequenza con 2 Jolly.
	 * @param iValue
	 * @param INPUT
	 * @param max_jolly
	 * @return
	 */
	public void checkValidGamesValue(int iValue, String INPUT, int max_jolly){
		// se ho 2 carte e non posso aggingere nessun Jolly, lascio stare :)
		if(INPUT.length()<9 && max_jolly==0){
			System.out.println("nessun gioco valido");
			return;
		}
		if(max_jolly>0){
			for(int i=0;i<max_jolly;i++){
				INPUT = (INPUT+"|**");
			}
		}
		StringBuffer listCards = new StringBuffer();
		String[] cardsValue = INPUT.split("\\|");
		for (String card : cardsValue) {
			if(listCards.indexOf(card)<0){
				listCards.append(card+"|");
			}
		}
		// se c'? una carta per ogni seme, allora devo generare anche tutte le altre combinazioni!
		if(listCards.toString().length()==12){
			String combinazione = listCards.toString();
			all_valid_games_value[iValue].put(combinazione,combinazione);
			String temp1 = null;
			String temp2 = null;
			for(int i=0;i<4;i++){
				temp1 = combinazione.substring(0,3);
				temp2 = combinazione.substring(3);
				all_valid_games_value[iValue].put(temp2,temp2);
				combinazione = temp2+temp1;
			}
		}
		else{
			all_valid_games_value[iValue].put(listCards.toString(),listCards.toString());
		}

		/*** metodo vecchio ***
		int i_step = 3;
		INPUT = INPUT.replaceAll("[*]", "");	// se ci sono dei Jolly in mezzo alle carte li tolgo
		if(INPUT.length()<4){
			INPUT = (INPUT+"****".substring(0, max_jolly)); // e li aggiungo al fondo 
		}
		int i_length = INPUT.length();
		if(i_length<i_step){
			return;
		}
		String REGEX = REGEX_VALUE.replace("@J", ""+(max_jolly+1));
		Pattern p = Pattern.compile(REGEX);
		int i_start = 0;
		String tmp_INPUT;
		for(int j=i_step;j<=i_length;j++){
			for(int i=i_start;i<=i_length-j;i++){
				tmp_INPUT = INPUT.substring(i,i+j);
				Matcher m = p.matcher(tmp_INPUT); // get a matcher object
				while(m.find()) {
					all_valid_games_value[iValue].put(_convert2FullFormatValue(m.group(0),iValue), _convert2FullFormatValue(m.group(0),iValue));
					int indexGroup = INPUT.indexOf(m.group(0));
					String INPUT2 = INPUT.substring(indexGroup+m.group(0).length());
					checkValidGamesValue(iValue,INPUT2, max_jolly);
				}
			}
		}
		 ***/
		return;
	}

	/***
	 * Dalla stringa di tutte le carte in mano al giocatore crea l'array con le carte rimanenti.
	 * @param carteDaTogliere - le carte giocate e quindi da non considerare pi?.
	 * @return
	 */
	private Card[] _playerCardsOnString2MyCards(Integer iSuit, String carteInMano, String carteDaTogliere){
		if(carteInMano == null || carteInMano.length() == 0){
			return null;
		}
		String[] splitString = carteInMano.split("\\|");
		//		Card[] result = new Card[splitString.length];
		Card[] result = new Card[14]; // devono essere sempre 14!
		Card card = null;
		for (String item : splitString) {
			card = new Card();
			card.value = item.substring(0,1);
			card.suit = item.substring(1,2);
			if(iSuit == null || _suit2Index(card.suit) == iSuit){
				int index = _value2Index(card.value);
				if(carteDaTogliere != null && carteDaTogliere.indexOf(card.value)>=0){
					carteDaTogliere = carteDaTogliere.replace(card.value, "");
					card = null;
				}
				result[index] = (index!=-1) ? card : null;
			}
		}
		return result;
	}

	/***
	 * Recupera la rappresentazione in formato Stringa di un seme in formato "compresso"(senza le doppie)
	 * A234**78*X*QK
	 * Il risultato viene passato alla regexp.
	 * @param carteInMano seme della carta
	 * @return
	 */
	private String _getCarteInManoDivisePerSemeTogliendoLaSequenzaGiocataInFormatoCompresso(int iSuit, String carteInMano, String sequenzaGiocata) {
		Card[] carteInManoDivisePerSeme = _playerCardsOnString2MyCards(iSuit,carteInMano,sequenzaGiocata);
		if(carteInManoDivisePerSeme == null || carteInManoDivisePerSeme.length == 0){
			return "";
		}
		StringBuffer result = new StringBuffer();
		Card c = null;
		for (int i=0;i<14;i++) {
			c = carteInManoDivisePerSeme[i];
			if(null!=c){
				result.append(c.value);
			}
			else{
				result.append(" ");
			}
		}
		return result.toString();
	}

	/***
	 * Recupera la rappresentazione in formato Stringa di un seme in formato "completo"(anche le doppie).
	 * Aggiunge un Jolly se la carta non ? disponibile.
	 * Il risultato viene passato alla regexp.
	 * @param carteInMano seme della carta
	 * @return
	 */
	private String _getCarteInManoDivisePerSemeTogliendoLaSequenzaGiocataInFormatoCompleto(int iSuit, String carteInMano, String carteDaTogliere) {
		String[] splitString = carteInMano.split("\\|");
		StringBuffer result = new StringBuffer();
		String value,suit=null;
		for (String item : splitString) {
			value = item.substring(0,1);
			suit = item.substring(1,2);
			if(_suit2Index(suit)==iSuit && carteDaTogliere.indexOf(value)>=0){
				carteDaTogliere = carteDaTogliere.replace(value, "");
			}
			else {
				result.append(value+suit+"|");
			}
		}
		return result.toString();
	}

	private String _getCardStringByValueArray(Card[] cardsValue) {
		StringBuffer result = new StringBuffer();
		for (int i=0;i<4;i++) {
			if(null != cardsValue[i]){
				result.append(cardsValue[i].value+cardsValue[i].suit+"|");
			}
		}
		return result.toString();
	}

	private int _suit2Index(String suit){
		return suitMap.get(suit);
	}

	private String _index2Suit(int index){
		return suitMap2.get(index);
	}

	private int _value2Index(String value){
		return valueMap.get(value);
	}

	private List<String> _Hashtable2List(Hashtable<String, String> hashtable){
		List<String> temp = Collections.list(hashtable.elements());
		Collections.sort(temp);
		return temp;
	}

	// ===================================
	// 				STAMPA
	// ===================================
	private void _printArrayCard(Card[] carte){
		// stampiamo il risultato almeno in ordine... :)
		for (Card carta : carte) {
			System.out.print(carta+"|");		
		}
		System.out.print("\n");
	}

	private void _printResult(Hashtable<String, String> hashtable){
		// stampiamo il risultato almeno in ordine... :)
		List<String> temp = Collections.list(hashtable.elements());
		Collections.sort(temp);
		for (String value : temp) {
			System.out.println("-->"+value);		
		}
	}

	/***
	 * stama tutti i giochi validi delle carte con lo stesso seme
	 * @param iSuit
	 */
	public void printAllValidGamesSuit2(int iSuit){
		System.out.println("================ printAllValidGamesSuit2:seme "+_index2Suit(iSuit));
		Hashtable<String,String> temp = all_valid_games_suit2[iSuit];
		ArrayList<String> list_temp = Collections.list(temp.elements());
		for (String item : list_temp) {
			System.out.print("VALID GAME=>"+item+"\n");
		}
		System.out.println("TOTALE SOUZIONI POSSIBILI="+temp.size());
	}

	public void printAllValidGamesValue(int iValue){
		Hashtable<String,String> temp = all_valid_games_value[iValue];
		List<String> list_temp = Collections.list(temp.elements());
		System.out.print("VALID GAME=>");
		for (String value : list_temp) {
			System.out.print(value+"|");
		}
		System.out.println("\n2:TOTALE SOUZIONI POSSIBILI valore "+(iValue+1)+"="+temp.size());
	}

	public void printListValidGamesValue(List<String> listGame){
		counter++;
		System.out.print(counter+" - "+listGame.size()+" ADD GAME =>");
		for (String value : listGame) {
			System.out.print(value+"|");
		}
		//		System.out.print("\n");
	}

	public String listString2StringSuit(List<String> list, int iSuit){
		StringBuffer r = new StringBuffer();
		for (String value : list) {
			r.append(_convert2FullFormatSuit(value,iSuit)+"|");// FIXME:prima c'era "#"
		}
		return r.toString();
	}

	//	public String listString2StringValue(List<String> list, int iValue){
	//		StringBuffer r = new StringBuffer();
	//		for (String value : list) {
	//			r.append(_convert2FullFormatValue(value,iValue)+"#");
	//		}
	//		return r.toString();
	//	}

	public List<List<Gioco>> estraiMigliorGiocoPossibile(List<Card> playerCards) {
		checkPlayerCards(playerCards); // estraggo tutti i possibili giochi dalla mano
		System.out.println("Ho "+playerCards.size() + " carte in mano..."); 
		String tutteLeCarteInMano = _getCarteInManoInFormatoCompleto(playerCards);
		_combinaGiochiPossibiliPerTuttiISemiEValori(tutteLeCarteInMano,0,0);
		//		all_valid_games_value
		//		printAllValidGamesSuit(iSuit);
		//		printAllValidGamesValue(iValue);
		return listaGiochiTutti;
	}

	private String _convert2FullFormatSuit(String item, int i) {
		char[] chars = item.toCharArray();
		StringBuffer result = new StringBuffer();
		for (char c : chars) {
			result.append(c+_index2Suit(i)+"|");	
		}
		return result.substring(0, result.length()-1);
	}

//	private String _convert2FullFormatValue(String item, int i) {
//		char[] chars = item.toCharArray();
//		StringBuffer result = new StringBuffer();
//		for (char c : chars) {
//			//			result.append(c+_index2Value(i)+"|");	
//		}
//		return result.substring(0, result.length()-1);
//	}

	public List<Gioco> listaGiochi = new ArrayList<Gioco>();
	public List<List<Gioco>> listaGiochiTutti = new ArrayList<List<Gioco>>();
	StringBuffer tempString = new StringBuffer();
	private void _combinaGiochiPossibiliPerTuttiISemiEValori(String tutteLeCarteInMano,int iSuit, int iValue){
		System.out.println(iSuit+"-"+iValue);
		if(iSuit<4){
			if(allGamesForSuit[iSuit].size() > 0){
				for (int i=0; i<allGamesForSuit[iSuit].size();i++) {
					String carteGiocoCorrenteSuit = allGamesForSuit[iSuit].get(i);
//					System.out.println("carteGiocoCorrenteSuit:"+carteGiocoCorrenteSuit);
					int start = tempString.length();
					int end   = tempString.length()+carteGiocoCorrenteSuit.length();
					tempString.append(carteGiocoCorrenteSuit);
					// Se le carte del mio gioco sono pi? di quelle che ho in mano, allora non ? un gioco valido...
					if(tempString.toString().length() > tutteLeCarteInMano.length() || !_isValidGame(tempString.toString(),tutteLeCarteInMano)){
						tempString.delete(start, end); // rimuove l'ultima sequenza inserita.
						continue;
					}
					else{
//						listaPossibileGioco.add(tempString.toString().replaceAll("#", "|"));
						Gioco gioco = new Gioco(carteGiocoCorrenteSuit);
						listaGiochi.add(gioco);
						_combinaGiochiPossibiliPerTuttiISemiEValori(tutteLeCarteInMano,iSuit+1,iValue);
					}
				}
			}
			else{
				// Se passo di qui non ci sono giochi possibili per questo seme...
				// ma potrebbero essercene per quello dopo...
				Gioco gioco = new Gioco(null);	// aggiungo comunque un gioco nullo
				listaGiochi.add(gioco);
				_combinaGiochiPossibiliPerTuttiISemiEValori(tutteLeCarteInMano,iSuit+1,iValue);
			}
		}
		else if(iSuit>=4 && iValue<13){
			if(allGamesForValue[iValue].size() > 0){
				for (int i=0; i<allGamesForValue[iValue].size();i++) {
					String carteGiocoCorrenteValue = allGamesForValue[iValue].get(i);
//					System.out.println("carteGiocoCorrenteValue:"+carteGiocoCorrenteValue);
					int start = tempString.length();
					int end   = tempString.length()+carteGiocoCorrenteValue.length();
					tempString.append(carteGiocoCorrenteValue);
					// Se le carte del mio gioco sono pi? di quelle che ho in mano, allora non ? un gioco valido...
					if(tempString.toString().length() > tutteLeCarteInMano.length() || !_isValidGame(tempString.toString(),tutteLeCarteInMano)){
						tempString.delete(start, end);
						continue;
					}
					else{
						Gioco gioco = new Gioco(carteGiocoCorrenteValue);
						listaGiochi.add(gioco);
						_combinaGiochiPossibiliPerTuttiISemiEValori(tutteLeCarteInMano,iSuit,iValue+1);
					}
				}
			}
			else{
				// Se passo di qui non ci sono giochi possibili per questo valore...
				// ma potrebbero essercene per quello dopo...
				Gioco gioco = new Gioco(null);	// aggiungo comunque un gioco nullo
				listaGiochi.add(gioco);
				_combinaGiochiPossibiliPerTuttiISemiEValori(tutteLeCarteInMano,iSuit,iValue+1);
			}
		}
		else{
			// beh, se arrivo qui ho finito e devo salvare tutta la sequenza fin qui raggiunta!
			// listaPossibileGioco.add(tempString.toString().replaceAll("#", "|"));
			listaGiochiTutti.add(_filtraGiochiNull(listaGiochi));
			System.out.println("=== FINITO:"+_filtraGiochiNull(listaGiochi).size());
		}
		if(listaGiochi.size()>0){
			Gioco g = listaGiochi.get(listaGiochi.size()-1);
			int start = tempString.length()-g.getNumeroCarte()*3;
			int end   = tempString.length();
			tempString.delete(start, end);
			listaGiochi.remove(listaGiochi.size()-1);
		}
		return;
	}

	/***
	 * Verifico se le carte utilizzate per un eventuale gioco sono le carte che ho in mano
	 * FIXME:qui potrei verificare se la sequenza che sto analizzando permette una CHUSURA(tutte le carte tranne 1!)
	 * @param carteGioco
	 * @param tutteLeCarteInMano
	 * @return
	 */
	private boolean _isValidGame(String carteGioco,String tutteLeCarteInMano) {
		String[] lCarte = carteGioco.split("\\|");
		for (String carta : lCarte) {
			if(tutteLeCarteInMano.indexOf(carta) < 0){
				return false;// la carta che voglio usare non c'?, gioco non valido!
			}
			tutteLeCarteInMano = tutteLeCarteInMano.replace(carta+"|", "");// rimpiazza la carta trovata con una sequenza vuota
		}
		return true;
	}

	private ArrayList<Gioco> _filtraGiochiNull(List<Gioco> listaGiochi) {
		ArrayList<Gioco> result = new ArrayList<GameValidator.Gioco>();
		for (Gioco gioco : listaGiochi) {
			if(null!=gioco.carte){
				result.add(gioco);
			}
		}
		return result;
	}

	private String _list2String(List<String> game) {
		StringBuffer temp = new StringBuffer();
		for (String item : game) {
			temp.append(game);
		}
		return temp.toString();
	}


	/***
	 * Estrae le carte del giocatore e restituisce la stringa in formato completo, doppie comprese.
	 * @return
	 */
	private String _getCarteInManoInFormatoCompleto(List<Card> playerCards) {
		StringBuffer temp = new StringBuffer();
		for (Card item : playerCards) {
			temp.append(item.value+item.suit+"|");
		}
		return temp.toString();
	}

	enum TypeGame{SUIT,VALUE};
	public class Gioco{
		String carte = null;
		public Gioco(String carte){
			this.carte = carte;
		}
		public int getNumeroCarte(){
			int result = (null==carte) ? 0: this.carte.length()/3;
			return result;
		}
		public int getPuntiGioco(){
			return _calcolaValoreCarteGioco(carte);
		}
		
		/****
		 * Quanto vale ad esempio carteGiocoCorrente = "2C|3C|4C"?
		 * @param carteGiocoCorrente
		 * @return
		 */
		private int _calcolaValoreCarteGioco(String carteGiocoCorrente) {
			String[] lCarte = carteGiocoCorrente.split("\\|");
			TypeGame type = (lCarte[0].charAt(1) == lCarte[1].charAt(1)) && (lCarte[1].charAt(1) == lCarte[2].charAt(1)) ? TypeGame.SUIT:TypeGame.VALUE;
			int totale = 0;
			boolean first = true;
			for (String carta : lCarte) {
				int index = _value2Index(carta.substring(0, 1));
				switch (type) {
				case SUIT:
					if(index == 13 && !first){ // ASSO vale 11!
						totale += 11;
					}
					else{
						totale += index+1;
					}
					break;
				case VALUE:
					if(index == 13){ // ASSO vale 11!
						totale += 11;
					}
					else{
						totale += index+1;
					}
					break;
				}
				first = false;
			}
			return totale;
		}

	}
}