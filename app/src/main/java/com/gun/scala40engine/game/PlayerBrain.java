package com.gun.scala40engine.game;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.gun.scala40engine.data.deck.Card;


/********
 * Questo ? il cuore del gioco...controlla le carte in mano e cerca la migliore combinazione per chiudere.
 * @author stefano
 */
public class PlayerBrain extends Thread{
	public static int START_GAME = 1, STOP_GAME=-1;
	public static final String TAG = "PlayerBrain";
	Player player = null;
	Handler h = null;
			
	public PlayerBrain(Player player){
		this.player = player;
	}
	
	public void run() {
		Log.d(TAG,"=== START GAME:PLAYER "+player.id +" is ready to PLAY!===");
		Looper.prepare();
		h = new Handler(){
		    @Override
		    public void handleMessage(Message msg){
		        if(msg.what == START_GAME){
		        	_startGame();
		        }
		        else{
		        	_stopGame();
		        }
		    }
		};		
		Looper.loop();
	}
	
	public Handler getHandler() {
		return h;
	}
	
	private void _startGame() {
		Log.d(TAG,"startGame - PLAYER ID="+player.id);
		player.nextStatus();	// 1. CARTA_PESCATA...
		player.grabCardFromDeckAction();
		player.nextStatus();	// 2. ORGANIZZA_GIOCO...
		player.nextStatus();	// 3. VERIFICA_ATTACCHI...
		player.nextStatus();	// 4. VERIFICA_CHIUSURA...
		player.nextStatus();	// 5. CARTA_SCARTATA...
		try {
			this.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Card card = player.handCards.get(0); // - Scelgo una carta a caso...per ora la prima
		player.discardInTheWellAction(card);
	}
	
	private void _stopGame() {
		Log.d(TAG,"stopGame - PLAYER ID="+player.id);
	}
}
