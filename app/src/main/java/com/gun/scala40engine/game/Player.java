package com.gun.scala40engine.game;

import java.util.ArrayList;
import java.util.Observable;

import android.util.Log;

import com.gun.scala40engine.data.DeckManager;
import com.gun.scala40engine.data.deck.Card;
import com.gun.scala40engine.game.PlayerBrain;

public class Player{
	public static final String TAG = "Player";
	public int id = 0;
	public enum _PLAYER_STATUS_ {
	    DISTRIBUZIONE_CARTE_INIZIO, DISTRIBUZIONE_CARTE_FINE, CHECK_MIO_TURNO, CARTA_PESCATA, ORGANIZZA_GIOCO, VERIFICA_ATTACCHI, VERIFICA_CHIUSURA, CARTA_SCARTATA;
   	}
	public POSITION myPosition;			// TOP,DOWN,LEFT,RIGHT
	public String orientation = null; 	// orientamento della carta H=orizzontale, V=verticale 
	public _PLAYER_STATUS_ currentStatus = _PLAYER_STATUS_.DISTRIBUZIONE_CARTE_INIZIO;
	public ArrayList<Card> handCards = new ArrayList<Card>(14);
	public PlayerBrain pb = new PlayerBrain(this);

	public Player(int id) {
		this.id = id;
		if(!pb.isAlive()){
			pb.start();
		}
	}

	public int getNCardsInHand(){
		return handCards.size();
	}

	public void setStatusAndNotify(_PLAYER_STATUS_ nextStatus) {
		currentStatus = nextStatus;
	}
	
    public void nextStatus() {
    	if(currentStatus.ordinal() == _PLAYER_STATUS_.CARTA_SCARTATA.ordinal()){
    		currentStatus = _PLAYER_STATUS_.CHECK_MIO_TURNO;
    	}
    	else{
    		currentStatus = _PLAYER_STATUS_.values()[currentStatus.ordinal()+1];
    	}
    	Log.d(TAG,"*********** currentStatus:PLAYER "+id + " ->" + currentStatus);
    }

	/***
	 * Afferra una carta dal mazzo principale.
	 * Se vCard==null allora la carta viene distribuita al giocatore dal Mazziere(quindi senza che sia toccata dal giocatore)
	 * oppure viene pescata da un giocatore CPU
	 * Se vCard!=null allora vCard ? la carta pescata dal TALLONE e toccata dal giocatore umano.
	 */
	public Card grabCardFromDeckAction(){
		Log.d(TAG,"grabCardFromDeckAction - PLAYER ID = "+this.id);
		Card card = DeckManager.getFirstCardFromDeck();
		return card;
	}


	/***
	 * Afferra una carta dal pozzo.
	 */
	public Card grabCardFromWellAction(){
		Card card = DeckManager.getFirstCardFromWell();
		handCards.add(card);
		card.setInHand(this);
		return card;
	}

	/***
	 * Scarta una carta dal mazzo principale
	 */
	public void discardInTheWellAction(Card vcard){
	}

	/***
	 * Dispone le carte della mano.
	 */
	public void disposeMyCardsAction(){
		for (int position=0;position<handCards.size();position++) {
			Card card = handCards.get(position);
		}
	}

	public static enum POSITION{DOWN,LEFT,TOP,RIGHT};

	public POSITION getMyPosition(){
		switch (id) {
		case 0:
			myPosition = POSITION.DOWN;
			break;
		case 1:
			myPosition = POSITION.LEFT;
			break;
		case 2:
			myPosition = POSITION.TOP;
			break;
		case 3:
			myPosition = POSITION.RIGHT;
			break;
		}
		return myPosition;
	}

	private void showMyCards(){
		//		for (CardVisual vcard : handCards) {
		//			Log.d(TAG,vcard.card.toString());
		//		}
	}

	/***
	 * Gestisce il passaggio da uno stato all'altro
	 * - DISTRIBUZIONE_CARTE_INIZIO
	 * - DISTRIBUZIONE_CARTE_FINE
	 * - ATTENDI_TURNO
	 * - MIO_TURNO
	 * - CARTA_PESCATA
	 * - ORGANIZZA_GIOCO
	 * - VERIFICA_ATTACCHI
	 * - VERIFICA_CHIUSURA
	 * - CARTA_SCARTATA
	 */
	public void update(Observable observable, Object _status) {
		Log.d(TAG,"-PLAYER id:"+id+" current status is "+currentStatus+" - Notifica:"+_status);
		_PLAYER_STATUS_ status = (_PLAYER_STATUS_)_status;
		switch (status) {
		case DISTRIBUZIONE_CARTE_INIZIO:	// Fase distribuzione delle carte.
			break;
			
		case DISTRIBUZIONE_CARTE_FINE: 		// attesa del proprio turno		
			currentStatus = _PLAYER_STATUS_.CHECK_MIO_TURNO;
			disposeMyCardsAction();
			_checkTurn();
			break;
			
		case CHECK_MIO_TURNO:				// controllo se ? il mio turno
			_checkTurn();
			break;

		case CARTA_PESCATA:
			Log.d(TAG,"-PLAYER "+id+" status is "+currentStatus);
			break;
			
		case ORGANIZZA_GIOCO:
			Log.d(TAG,"-PLAYER "+id+" status is "+currentStatus);
			break;
			
		case VERIFICA_ATTACCHI:
			Log.d(TAG,"-PLAYER "+id+" status is "+currentStatus);
			break;
			
		case VERIFICA_CHIUSURA:
			Log.d(TAG,"-PLAYER "+id+" status is "+currentStatus);
			break;
			
		case CARTA_SCARTATA:		// FINE TURNO!
			Log.d(TAG,"-PLAYER "+id+" status is "+currentStatus);
			if(this.currentStatus == _PLAYER_STATUS_.CARTA_SCARTATA){
				this.nextStatus();
			}
			else{
				_checkTurn();
			}
			break;
		default:
			break;
		}
	}

	private void _checkTurn(){
	}

	public boolean isCardInMyHandle(Card card) {
		for (int i = 0;i<handCards.size();i++) {
			Card card2 = handCards.get(i);
			if(card.equals(card2)){
				return true;
			}
		}
		return false;
	}
}