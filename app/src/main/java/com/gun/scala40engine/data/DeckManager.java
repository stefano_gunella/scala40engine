package com.gun.scala40engine.data;

import com.gun.scala40engine.data.deck.Card;

import java.util.ArrayList;
import java.util.Random;

public class DeckManager {
	private static final String[] semi = {"C","Q","P","F"};
	private static final String[] values = {"A","2","3","4","5","6","7","8","9","X","J","Q","K"};
	public static ArrayList<Card> mainDeck = null;

	/***
	 * @param nDeck
	 * @param shuffle
	 */
	public static void makeDeck(int nDeck, boolean shuffle){
		ArrayList<Card> result = new ArrayList<Card>();
		// aggiungo tutte le carte...
		for(int i=0;i<nDeck;i++){
			for (String suit : semi) {
				for (String value : values) {
					Card card = new Card(i, suit, value, i%nDeck, null);
					result.add(card);
				}
			}
		}
		// aggiungo tutti i Jolly
		for(int i=0;i<nDeck;i++){
			for (int j=0; j<2; j++) {
				Card card = new Card(i, "*", "*", i%nDeck, null);
				result.add(card);
			}
		}
		mainDeck = result;
		// mischio...
		if(shuffle){
			shuffleCards(100);
		}
	}

	public static void shuffleCards(int nPassaggi){
		ArrayList<Card> result = new ArrayList<Card>();
		int deckSize;
		for(int i=0;i<nPassaggi;i++){
			while ((deckSize = mainDeck.size()) > 0) {
				Card c = mainDeck.remove(new Random().nextInt(deckSize));
				result.add(c);
			}
			mainDeck = (ArrayList<Card>)(result.clone());
			result.clear();
		}
	}

	public static Card getFirstCardFromDeck(){
		return null;
	}

	public static Card getFirstCardFromWell(){
		return null;
	}
}
