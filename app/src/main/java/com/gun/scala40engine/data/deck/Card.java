package com.gun.scala40engine.data.deck;

import com.gun.scala40engine.game.Player;
// Carata da gioco
public class Card {
	public String suit = null;			// seme cuori,quadri,picche,fiori,*(JOLLY)
	public String value = null;			// valore A,1,2,3...,J,Q,K
	public int deck = 0;				// mazzo 1 o mazzo 2
	public int backColor = 0;			// colore retro della carta Rosso/Blu
	public boolean isDrawable = false;	// la carta puo' essere toccata dal giocatore UMANO.
	public boolean isInDeck = true;		// la carta e' nel mazzo principare(TALLONE)
	public boolean isInHand = false;	// la carta e' in mano ad un giocatore
	public boolean isInGame = false;	// la carta fa parte di un "gioco"(una qualsiasi combinazione accettabile)
	public boolean isInWell = false;	// la carta e' stata scartata
	public Player player = null;		// il giocatore che possiede la carta o l'ultimo che l'ha giocata.

	public Card(){
	}

	public Card(int deck, String seme, String value) {
		this.deck 	= deck;
		this.suit 	= seme;
		this.value 	= value;
	}

	public Card(int deck, String seme, String value, int backColor, Player player) {
		this.deck 		= deck;
		this.suit 		= seme;
		this.value 		= value;
		this.backColor 	= backColor;
		this.player 	= player;
	}

	public void setInHand(Player p){
		isInDeck 	= false;	// non e' nel mazzo
		isInHand 	= true;		// e' in mano al giocatore
		isDrawable 	= false;	// non e' toccabile dagli altri giocatori
		isInGame 	= false;	// non fa parte di un gioco
		isInWell 	= false;	// non e' stata scartata
		player 		= p;		// assegna il giocatore
	}
	
	public void setInWell(){
		isInDeck 	= false;	// non e' nel mazzo
		isInHand 	= false;	// non e'� in mano al giocatore
		isDrawable 	= true;		// e' toccabile dat tutti i giocatori
		isInGame 	= false;	// non fa parte di un gioco
		isInWell 	= true;		// e' stata scartata
	}
	
	public void setInGame(){
		isInDeck 	= false;	// non e nel mazzo
		isInHand 	= false;	// non ein mano al giocatore
		isDrawable 	= true;		// e toccabile
		isInGame 	= true;		// fa parte di un gioco
		isInWell 	= false;	// non e stata scartata
	}

	public boolean equals(Card c2){
		boolean result = (this.suit == c2.suit) && (this.value == c2.value) && (this.deck == c2.deck);
		return result;
	}

	// Valore e seme della carta
	public String getCardValueSuit() {
		return "["+value+suit+"]";
	}
	
	@Override
	public String toString() {
		return "["+value+suit+"|"+deck+"]";
	}
}
