package com.gun.scala40;

import com.gun.scala40engine.data.DeckManager;
import com.gun.scala40engine.data.deck.Card;
import com.gun.scala40engine.utils.GameValidator;
import com.gun.scala40engine.utils.JsonManager;

import org.junit.Test;

import java.util.Hashtable;
import java.util.List;

import static org.junit.Assert.*;

public class Scala40Test {
    @Test
    public void firstTest() {
        DeckManager.makeDeck(2,true);
        assertNotNull(DeckManager.mainDeck);
    }

    /**
     * Il formato delle carte deve essere del tipo:
     * [valore][seme]|[valore][seme]
     * Quindi la sequenza dei FIORI potrebbe essere:
     * Es:AF|2F|3F|4F|5F|6F|7F|8F|9F|XF|JF|QF|KF|@F
     * Test testCheckValidGames
     */
    @Test
    public void testCheckValidGamesSuit(){
        String INPUT0 = "AF|2F|3F|4F|5F|6F|7F";
        String INPUT1 = "AF|2F|3F|4F|5F|6F|7F|8F|9F|XF|JF|QF|KF|@F";
        String INPUT2 = "AF|2F|3F|5F";
        String INPUT3 = "2F|3F|4F|2F|3F|4F";
        String INPUT4 = "2F|4F|6F|@F";
        String INPUT5 = "AF|2F|4F|5F|6F"; // TODO:questa sequenza deve avere 1 sola soluzione se non ci sono JOLLY
        String INPUT6 = "AF|2F|4F|5F|6F|7F|8F|9F|XF|JF|QF|KF|@F";
        String INPUT7 = "4F|5F|6F|7F|8F|9F|XF|JF|QF|KF|@F";
        String INPUT8 = "4F|5F|6F|7F|8F";
        String INPUT9 = "2C|3C|4C|5C|5Q|4P|5P|6P|7P|8P|5F|";
        int selectedSuit = 3;	// FIORI
        GameValidator gv = new GameValidator();
        gv.checkValidGamesSuit(selectedSuit,INPUT1,0,true); // controllo per Fiori(3), nessun Jolly ammesso, rimpiazza le carte mancanti con Jolly
//		gv.printAllValidGamesSuit(selectedSuit);
        gv.printAllValidGamesSuit2(selectedSuit);
        assertTrue(gv.all_valid_games_suit2.length > 0);
    }

    /**
     * Il formato delle carte deve essere del tipo:
     * [valore][seme]|[valore][seme]
     * Quindi una sequenza potrebbe essere:
     * Es:AC|AQ|AP|AF
     * Test testCheckValidGames
     */
    public void testCheckValidGamesValue() throws Throwable {
        String INPUT1 = "AC|AQ|AP|AF";
        String INPUT2 = "AC|AF";
        String INPUT3 = "JC|JQ|JF";
        String INPUT4 = "@C|@Q|@P|@F";
        String INPUT5 = "AC|AQ|AC|AF";
        String INPUT6 = "AC|AQ|AF";
        GameValidator gv = new GameValidator();
        gv.checkValidGamesValue(0,INPUT2,0);
//		gv.printAllValidGamesValue(1);
        assertTrue(gv.all_valid_games_value.length > 0);
    }

    public void testCheckPlayerCards_RANDOM() throws Throwable {
        System.out.println("=========== testCheckPlayerCards_RANDOM =============");
        DeckManager.makeDeck(2, true);
        List<Card> randomCards = DeckManager.mainDeck.subList(0, 14);
        GameValidator gv = new GameValidator();
        System.out.println("estraggo "+randomCards.size() + " dal mazzo");
        gv.checkPlayerCards(randomCards);
        assertTrue(true);
    }

    public void testCheckPlayerCards_JSON() throws Throwable {
        System.out.println("=========== testCheckPlayerCards_JSON ===============");
        JsonManager jm = new JsonManager();
        List<Card> playerCards = jm.readJSON("data/json/gioco_chiusura_in_mano_02.json");
        GameValidator gv = new GameValidator();
        gv.checkPlayerCards(playerCards);
        assertTrue(true);
    }

    public void testEstraiMigliorGiocoPossibile() throws Throwable {
        System.out.println("=========== testEstraiMigliorGiocoPossibile ===============");
        JsonManager jm = new JsonManager();
        List<Card> myCards = jm.readJSON("data/json/gioco_chiusura_in_mano_02.json");
        GameValidator gv = new GameValidator();
//		qui devo fare tutte le possibili combinazioni
        List<List<GameValidator.Gioco>> result = gv.estraiMigliorGiocoPossibile(myCards); // estrae il miglior gioco possibile in base alle carte in mano
        assertTrue(result.size() > 0);
    }

    public void testJsonManager_writeJSON() throws Throwable {
        System.out.println("=========== testJsonManager_writeJSON ===============");
        DeckManager.makeDeck(2, true);
        List<Card> randomCards = DeckManager.mainDeck.subList(0, 14);
        JsonManager jm = new JsonManager();
        jm.writeJSON(randomCards);
        assertTrue(true);
    }

    public void testJsonManager_readJSON() throws Throwable {
        System.out.println("=========== testJsonManager_readJSON ================");
        JsonManager jm = new JsonManager();
        List<Card> result = jm.readJSON("data/json/gioco_chiusura_in_mano.json");
        assertTrue(result.size()>0);
    }

    public void testHastable() throws Throwable {
        System.out.println("================== testHastable =====================");
        Hashtable<String, String> ht = new Hashtable<String, String>();
        ht.put("ciao", "Uno");
        ht.put("ciao", "Due");
        System.out.println("Size is "+ht.size());
        assertTrue(ht.size()==1);
    }
}